import 'package:flutter/material.dart';

class TasksWidget extends StatefulWidget {
  int index;

  TasksWidget({Key? key, required this.index}) : super(key: key);

  @override
  State<TasksWidget> createState() => _TasksWidgetState();
}

class _TasksWidgetState extends State<TasksWidget> {
  bool isOpen = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: 585,
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.black),
            borderRadius: BorderRadius.circular(20),
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 22),
                  Padding(
                    padding: EdgeInsets.only(left: 30, right: 30),
                    child: Row(
                      children: [
                        const Text(
                          'Задача',
                          style: TextStyle(
                              color: Color.fromRGBO(103, 103, 103, 1),
                              fontSize: 48),
                        ),
                        const Spacer(),
                        GestureDetector(
                          child: Icon(Icons.navigation),
                          onTap: () {
                            setState(() {
                              isOpen = !isOpen;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 17),
                  Divider(color: Colors.black),
                  SizedBox(height: 29),
                  Column(
                    children: List.generate(
                      widget.index,
                      (index) => Padding(
                        padding: EdgeInsets.only(bottom: 17),
                        child: Container(
                          width: 543,
                          height: 137,
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                            color: Color.fromRGBO(243, 243, 243, 1),
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(left: 9),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  'Доделать дизайн',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 48),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  'Тут должен быть предпросмотр задачи, но мне слишком лень его придумывать...',
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 24),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              if (isOpen) ...[
                Padding(
                  padding: EdgeInsets.only(top: 103),
                  child: Container(
                    width: 585,
                    height: 370,
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.black),
                        borderRadius: BorderRadius.only(
                          bottomLeft: const Radius.circular(20),
                          bottomRight: const Radius.circular(20),
                        ),
                        color: Colors.white),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: 25,
                        top: 25,
                      ),
                      child: Column(
                        children: List.generate(
                          4,
                          (index) => Padding(
                            padding: EdgeInsets.only(bottom: index == 3 ? 0 : 38),
                            child: Row(
                              children: [
                                Icon(index == 0
                                    ? Icons.navigation
                                    : index == 1
                                        ? Icons.copy
                                        : index == 2
                                            ? Icons.info
                                            : Icons.delete, color: Color.fromRGBO(96, 112, 255, 1), size: 45,),
                                SizedBox(width: 23),
                                Text(
                                  index == 0
                                      ? 'Добавить карточку'
                                      : index == 1
                                          ? 'Добавить карточку'
                                          : index == 2
                                              ? 'Информация'
                                              : 'Удалить задачу',
                                  style: TextStyle(
                                    fontSize: 44,
                                    color:
                                        index == 3 ? Colors.red : Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ],
          ),
        ),
      ),
    );
  }
}
