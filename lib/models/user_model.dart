class UserModel {
  UsersModel? users;

  UserModel({this.users});

  UserModel.fromJson(Map<String, dynamic> json) {
    users = json['users'] != null ? UsersModel?.fromJson(json['users']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};
    if (users != null) {
      json['users'] = users!.toJson();
    }
    return json;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}


class UsersModel {
  int? id;
  String? fullname;
  dynamic email;
  dynamic info;

  UsersModel({this.id, this.fullname, this.email, this.info});

  UsersModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullname = json['fullname'];
    email = json['email'];
    info = json['info'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};
    json['id'] = id;
    json['fullname'] = fullname;
    json['email'] = email;
    json['info'] = info;
    return json;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}


