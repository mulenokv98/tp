class GroupModel {
  GroupsModel? groups;

  GroupModel({this.groups});

  GroupModel.fromJson(Map<String, dynamic> json) {
    groups =
    json['groups'] != null ? GroupsModel?.fromJson(json['groups']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};
    if (groups != null) {
      json['groups'] = groups!.toJson();
    }
    return json;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}


class GroupsModel {
  int? id;
  String? name;
  bool? private;

  GroupsModel({this.id, this.name, this.private});

  GroupsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    private = json['private'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};
    json['id'] = id;
    json['name'] = name;
    json['private'] = private;
    return json;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}


