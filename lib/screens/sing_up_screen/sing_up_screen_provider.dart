import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'sing_up_screen.dart';
import 'cubit/cb_sing_up_screen.dart';
import 'cubit/st_sing_up_screen.dart';

class SingUpScreenProvider extends StatelessWidget {
  const SingUpScreenProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CbSingUpScreen>(
      create: (context) => CbSingUpScreen(),
      child: const SingUpScreen(),
    );
  }
}    
    