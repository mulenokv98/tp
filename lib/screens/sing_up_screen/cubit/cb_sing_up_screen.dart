import 'dart:developer';

import 'package:get_storage/get_storage.dart';

import 'st_sing_up_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as httpClient;
import 'dart:convert' as convert;

class CbSingUpScreen extends Cubit<StSingUpScreen> {
  CbSingUpScreen() : super(StSingUpScreenLoaded());

  Future<void> setUsers(String login, String password, String email) async {
    emit(StSingUpScreenLoading());
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8",
    };
    var response = await httpClient.post(
      Uri(
        scheme: 'http',
        host: '192.168.164.78',
        path: 'api/v1/users',
      ),
      body: convert.jsonEncode(
        {
          'login': login,
          'password': password,
          'email': email,
        },
      ),
      headers: headers,
    );
    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;
    if (jsonResponse['status'] == 200){
      var response = await httpClient.post(
        Uri(
          scheme: 'http',
          host: '192.168.164.78',
          path: 'login',
        ),
        body: convert.jsonEncode(
          {
            'login': login,
            'password': password,
          },
        ),
        headers: headers,
      );
      GetStorage().write('userId', response.body);
      emit(StSingUpScreenPushMain());
    }
  }
}
