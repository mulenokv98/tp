abstract class StSingUpScreen{}

class StSingUpScreenInit extends StSingUpScreen{}

class StSingUpScreenLoaded extends StSingUpScreen{}

class StSingUpScreenLoading extends StSingUpScreen{}

class StSingUpScreenNoAuthError extends StSingUpScreen{}

class StSingUpScreenNoInternetError extends StSingUpScreen {}

class StSingUpScreenPushMain extends StSingUpScreen {}

class StSingUpScreenError extends StSingUpScreen{
  final int? error;
  final String? message;
  StSingUpScreenError({this.error,this.message});
}
    