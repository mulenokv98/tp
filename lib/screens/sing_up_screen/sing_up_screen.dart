import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:tp/screens/groups_screen/groups_screen_provider.dart';
import 'package:tp/widgets/Creating_group/form.dart';
import 'package:tp/widgets/button.dart';
import 'package:tp/widgets/text_styles.dart';
import 'cubit/cb_sing_up_screen.dart';
import 'cubit/st_sing_up_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SingUpScreen extends StatefulWidget {
  const SingUpScreen({Key? key}) : super(key: key);

  @override
  _SingUpScreenState createState() => _SingUpScreenState();
}

class _SingUpScreenState extends State<SingUpScreen> {
  TextEditingController _loginController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: BlocConsumer<CbSingUpScreen, StSingUpScreen>(
          listener: (context, state) {
            if (state is StSingUpScreenPushMain) {
              Navigator.of(context).push(
                CupertinoPageRoute(
                  builder: (BuildContext context) {
                    return const GroupsScreenProvider();
                  },
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is StSingUpScreenLoading) {
              return Column(
                children: const [
                  Center(
                    child: CupertinoActivityIndicator(),
                  ),
                ],
              );
            }
            if (state is StSingUpScreenLoaded) {
              return Column(
                children: [
                  SizedBox(height: 250.w),
                  Center(
                    child: Text(
                      'SING UP',
                      style: PjTextStyles.auth,
                    ),
                  ),
                  SizedBox(height: 30.w),
                  FormWidget(
                    controller: _loginController,
                    hintText: 'login',
                  ),
                  SizedBox(height: 30.w),
                  FormWidget(
                    controller: _passwordController,
                    hintText: 'password',
                  ),
                  SizedBox(height: 30.w),
                  FormWidget(
                    controller: _confirmPasswordController,
                    hintText: 'confirm password',
                  ),
                  SizedBox(height: 30.w),
                  FormWidget(
                    controller: _emailController,
                    hintText: 'email',
                  ),
                  SizedBox(height: 150.w),
                  PjButton(
                    text: 'SING UP',
                    onTap: () {
                      BlocProvider.of<CbSingUpScreen>(context).setUsers(
                          _loginController.text,
                          _passwordController.text,
                          _emailController.text);
                    },
                    isWhiteButton: true,
                  ),
                ],
              );
            }
            if (state is StSingUpScreenError) {
              return Container(color: Colors.red);
            }
            return Container(color: Colors.grey);
          },
        ),
      ),
    );
  }
}
