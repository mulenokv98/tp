import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'groups_screen.dart';
import 'cubit/cb_groups_screen.dart';
import 'cubit/st_groups_screen.dart';

class GroupsScreenProvider extends StatelessWidget {
  const GroupsScreenProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CbGroupsScreen>(
      create: (context) => CbGroupsScreen(),
      child: const GroupsScreen(),
    );
  }
}    
    