import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:tp/screens/creating_group_screen/creating_group_screen_provider.dart';
import 'package:tp/widgets/Creating_group/AppBar_V2_widget.dart';
import 'package:tp/widgets/Creating_group/creating_group.dart';
import 'package:tp/widgets/drawer_widget.dart';
import 'package:tp/widgets/group_widget.dart';
import 'package:tp/widgets/pj_colors.dart';
import 'package:tp/widgets/text_styles.dart';
import '../../widgets/floating_button_widget.dart';
import 'cubit/cb_groups_screen.dart';
import 'cubit/st_groups_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GroupsScreen extends StatefulWidget {
  const GroupsScreen({Key? key}) : super(key: key);

  @override
  _GroupsScreenState createState() => _GroupsScreenState();
}

class _GroupsScreenState extends State<GroupsScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: PjAppBar(
          title: 'Группы',
        ),
        body: BlocBuilder<CbGroupsScreen, StGroupsScreen>(
          builder: (context, state) {
            if (state is StGroupsScreenLoading) {
              return const Center(
                child: CupertinoActivityIndicator(),
              );
            }
            if (state is StGroupsScreenInit) {
              BlocProvider.of<CbGroupsScreen>(context).getGroups();
              return Container();
            }
            if (state is StGroupsScreenLoaded) {
              return Column(
                children: state.groups.length != 0
                    ? List.generate(
                        state.groups.length,
                        (index) => Padding(
                          padding: EdgeInsets.only(top: 40.w),
                          child: GroupWidget(
                            group: state.groups[index],
                          ),
                        ),
                      )
                    : [
                        SizedBox(height: 50.w),
                        Center(
                          child: SizedBox(
                            width: 450.w,
                            child: Text(
                              'У вас еще нет групп. Создайте чо нибудь, пожалуйста',
                              style: TextStyle(color: PjColors.getGrey600, fontSize: 30),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      ],
              );
            }
            if (state is StGroupsScreenError) {
              return Container(color: Colors.red);
            }
            return Container(color: Colors.grey);
          },
        ),
        drawer: DrawerWidget(),
        floatingActionButton: FloatingButtonWidget(
          onPressed: () {
            Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (BuildContext context) {
                  return CreatingGroupScreenProvider();
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
