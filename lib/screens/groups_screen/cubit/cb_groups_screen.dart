import 'dart:developer';

import 'package:get_storage/get_storage.dart';
import 'package:tp/models/groups_model.dart';
import 'package:tp/models/user_model.dart';

import 'st_groups_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as httpClient;
import 'dart:convert' as convert;

class CbGroupsScreen extends Cubit<StGroupsScreen> {
  CbGroupsScreen() : super(StGroupsScreenInit());

  Future<void> getGroups() async {
    var response = await httpClient.get(
      Uri(
        scheme: 'http',
        host: '192.168.164.78',
        path: 'api/v1/users/${GetStorage().read('userId')}/groups',
      ),
    );
    var jsonResponse =
    convert.jsonDecode(response.body) as Map<String, dynamic>;
    List<GroupsModel> groups = [];
    for (var group in jsonResponse['groups']){
      groups.add(GroupsModel.fromJson(group));
    }
    emit(StGroupsScreenLoaded(groups: groups));
  }

  Future<void> deleteGroups(int groupId) async {
    var response = await httpClient.delete(
      Uri(
        scheme: 'http',
        host: '192.168.164.78',
        path: 'api/v1/users/${GetStorage().read('userId')}/groups/${groupId}',
      ),
    );
    var jsonResponse =
    convert.jsonDecode(response.body) as Map<String, dynamic>;
    if (jsonResponse['status'] == 200){
      getGroups();
    }
  }
// Future<void> setUsers() async {
//   Map<String, String> headers = {
//     "Content-Type": "application/json; charset=UTF-8",
//   };
//   var response = await httpClient.post(
//     Uri(
//       scheme: 'http',
//       host: '192.168.164.78',
//       path: 'api/v1/users',
//     ),
//     body: convert.jsonEncode({
//       'login': 'abobuss',
//       'password': 'qwerty',
//       'email': 'abobuss@domru.com',
//     }),
//     headers: headers,
//   );
//   var jsonResponse =
//   convert.jsonDecode(response.body) as Map<String, dynamic>;
//   log(jsonResponse['status']);
// }
}
