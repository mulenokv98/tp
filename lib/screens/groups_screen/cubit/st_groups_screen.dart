import 'package:tp/models/groups_model.dart';

abstract class StGroupsScreen{}

class StGroupsScreenInit extends StGroupsScreen{}

class StGroupsScreenLoaded extends StGroupsScreen{
  List<GroupsModel> groups;

  StGroupsScreenLoaded({required this.groups});
}

class StGroupsScreenLoading extends StGroupsScreen{}

class StGroupsScreenNoAuthError extends StGroupsScreen{}

class StGroupsScreenNoInternetError extends StGroupsScreen {}

class StGroupsScreenError extends StGroupsScreen{
  final int? error;
  final String? message;
  StGroupsScreenError({this.error,this.message});
}
    