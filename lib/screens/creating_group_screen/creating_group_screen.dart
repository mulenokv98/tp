import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/screens/groups_screen/groups_screen_provider.dart';
import 'package:tp/widgets/Creating_group/AppBar_V2_widget.dart';
import 'package:tp/widgets/Creating_group/form.dart';
import 'package:tp/widgets/button.dart';
import 'package:tp/widgets/pj_colors.dart';
import 'cubit/cb_creating_group_screen.dart';
import 'cubit/st_creating_group_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreatingGroupScreen extends StatefulWidget {
  int groupId;
  CreatingGroupScreen({Key? key, required this.groupId}) : super(key: key);

  @override
  _CreatingGroupScreenState createState() => _CreatingGroupScreenState();
}

class _CreatingGroupScreenState extends State<CreatingGroupScreen> {
  TextEditingController _controller = TextEditingController();
  bool isPublic = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PjAppBar(
        title: widget.groupId == -1 ? 'Добавление группы' : 'Редактирование группы',
      ),
      body: BlocConsumer<CbCreatingGroupScreen, StCreatingGroupScreen>(
        listener: (context, state) {
          if (state is StCreatingGroupScreenPushBack){
            Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (BuildContext context) {
                  return const GroupsScreenProvider();
                },
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is StCreatingGroupScreenLoading) {
            return const Center(
              child: CupertinoActivityIndicator(),
            );
          }
          if (state is StCreatingGroupScreenLoaded) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    left: 30.w,
                    top: 35.w,
                    bottom: 15.w,
                  ),
                  child: Row(
                    children: [
                      Container(
                        constraints: BoxConstraints(
                            minHeight: 80,
                            minWidth: 80,
                            maxHeight: 80,
                            maxWidth: 80),
                        decoration: BoxDecoration(
                          border: Border.all(color: PjColors.getGrey600),
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                          color: PjColors.getBGPColor,
                        ),
                        //child: Image(),
                      ),
                      SizedBox(width: 20.w),
                      FormWidget(
                        hintText: 'Название',
                        controller: _controller,
                        size: 470,
                      ),
                    ],
                  ),
                ),
                Divider(color: PjColors.getGrey600),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: (){
                    setState(() {
                      isPublic = true;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.all(25.w),
                    margin: EdgeInsets.all(40.w),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: PjColors.getGrey600),
                    ),
                    child: Column(
                      children: [
                        Text(
                          "Настройки приватности",
                          style: TextStyle(fontSize: 25),
                        ),
                        SizedBox(height: 30.w),
                        Container(
                          padding: EdgeInsets.only(
                              top: 5, left: 20.w, right: 20.w, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: isPublic ? PjColors.getDarkBlue : PjColors.getGrey600, width: isPublic ? 5.w : 3.w),
                          ),
                          child: Column(
                            children: [
                              Text("Публичная доска",
                                  style: TextStyle(
                                      fontSize: 25, fontWeight: FontWeight.w500)),
                              SizedBox(height: 5),
                              Text(
                                "Если вы выберите публичный тип, то другие участники смогут свободно просматривать эту доску",
                                style: TextStyle(
                                  fontSize: 17,
                                  color: PjColors.getGrey600,
                                ),
                              ),
                            ],
                            crossAxisAlignment: CrossAxisAlignment.start,
                          ),
                        ),
                        SizedBox(height: 20.w),
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: (){
                            setState(() {
                              isPublic = false;
                              log('false');
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 10.w, left: 20.w, right: 20.w, bottom: 20.w),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: isPublic ? PjColors.getGrey600 : PjColors.getDarkBlue, width: isPublic ? 3.w : 5.w),
                            ),
                            child: Column(
                              children: [
                                Text("Приватная доска",
                                    style: TextStyle(
                                        fontSize: 25, fontWeight: FontWeight.w500)),
                                SizedBox(height: 5),
                                Text(
                                  "Если вы выберите приватный тип, то другие участники не смогут свободно просматривать эту доску",
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: PjColors.getGrey600,
                                  ),
                                ),
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
                PjButton(text: widget.groupId == -1 ? 'Создать' : 'Редактировать', onTap: () {
                  if (widget.groupId == -1) {
                    BlocProvider.of<CbCreatingGroupScreen>(context).createGroup(_controller.text, !isPublic);
                  }
                  else{
                    BlocProvider.of<CbCreatingGroupScreen>(context).editGroup(_controller.text, !isPublic, widget.groupId);
                  }
                }),
                SizedBox(
                  height: 60.w,
                )
              ],
            );
          }
          if (state is StCreatingGroupScreenError) {
            return Container(color: Colors.red);
          }
          return Container(color: Colors.grey);
        },
      ),
    );
  }
}
