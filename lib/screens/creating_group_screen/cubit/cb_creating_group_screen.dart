import 'dart:developer';

import 'package:get_storage/get_storage.dart';

import 'st_creating_group_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as httpClient;
import 'dart:convert' as convert;

class CbCreatingGroupScreen extends Cubit<StCreatingGroupScreen> {
  CbCreatingGroupScreen() : super(StCreatingGroupScreenLoaded());

  Future<void> createGroup(String name, bool private) async {
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8",
    };
    var response = await httpClient.post(
      Uri(
        scheme: 'http',
        host: '192.168.164.78',
        path: 'api/v1/users/${GetStorage().read('userId')}/groups',
      ),
      body: convert.jsonEncode(
        {
          'name': name,
          'private': private,
        },
      ),
      headers: headers,
    );
    var jsonResponse =
    convert.jsonDecode(response.body) as Map<String, dynamic>;
    if (jsonResponse['status'] == 200){
      emit(StCreatingGroupScreenPushBack());
    }
  }

  Future<void> editGroup(String name, bool private, int groupId) async {
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8",
    };
    var response = await httpClient.put(
      Uri(
        scheme: 'http',
        host: '192.168.164.78',
        path: 'api/v1/users/${GetStorage().read('userId')}/groups/${groupId}',
      ),
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
        'name': name,
        'private': private.toString(),
      },
    );
    log("EDIT");
    var jsonResponse =
    convert.jsonDecode(response.body) as Map<String, dynamic>;
    if (jsonResponse['status'] == 200){
      emit(StCreatingGroupScreenPushBack());
    }
  }
}
    