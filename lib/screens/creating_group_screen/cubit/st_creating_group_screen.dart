abstract class StCreatingGroupScreen{}

class StCreatingGroupScreenInit extends StCreatingGroupScreen{}

class StCreatingGroupScreenLoaded extends StCreatingGroupScreen{}

class StCreatingGroupScreenLoading extends StCreatingGroupScreen{}

class StCreatingGroupScreenNoAuthError extends StCreatingGroupScreen{}

class StCreatingGroupScreenPushBack extends StCreatingGroupScreen{}

class StCreatingGroupScreenNoInternetError extends StCreatingGroupScreen {}

class StCreatingGroupScreenError extends StCreatingGroupScreen{
  final int? error;
  final String? message;
  StCreatingGroupScreenError({this.error,this.message});
}
    