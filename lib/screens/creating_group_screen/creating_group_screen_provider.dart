import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'creating_group_screen.dart';
import 'cubit/cb_creating_group_screen.dart';
import 'cubit/st_creating_group_screen.dart';

class CreatingGroupScreenProvider extends StatelessWidget {
  int groupId;
  CreatingGroupScreenProvider({Key? key, this.groupId = -1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CbCreatingGroupScreen>(
      create: (context) => CbCreatingGroupScreen(),
      child: CreatingGroupScreen(groupId: groupId,),
    );
  }
}    
    