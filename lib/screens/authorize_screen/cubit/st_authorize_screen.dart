abstract class StAuthorizeScreen{}

class StAuthorizeScreenInit extends StAuthorizeScreen{}

class StAuthorizeScreenLoaded extends StAuthorizeScreen{}

class StAuthorizeScreenLoading extends StAuthorizeScreen{}

class StAuthorizeScreenNoAuthError extends StAuthorizeScreen{}

class StAuthorizeScreenPushMain extends StAuthorizeScreen{}

class StAuthorizeScreenNoInternetError extends StAuthorizeScreen {}

class StAuthorizeScreenError extends StAuthorizeScreen{
  final int? error;
  final String? message;
  StAuthorizeScreenError({this.error,this.message});
}
    