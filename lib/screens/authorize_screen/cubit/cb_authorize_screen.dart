import 'dart:developer';

import 'package:get_storage/get_storage.dart';

import 'st_authorize_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as httpClient;
import 'dart:convert' as convert;

class CbAuthorizeScreen extends Cubit<StAuthorizeScreen> {
  CbAuthorizeScreen() : super(StAuthorizeScreenLoaded());

  Future<void> auth(String login, String password) async {
    emit(StAuthorizeScreenLoading());
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8",
    };
    var response = await httpClient.post(
      Uri(
        scheme: 'http',
        host: '192.168.164.78',
        path: 'login',
      ),
      body: convert.jsonEncode(
        {
          'login': login,
          'password': password,
        },
      ),
      headers: headers,
    );
    GetStorage().write('userId', response.body);
    emit(StAuthorizeScreenPushMain());
  }
}
    