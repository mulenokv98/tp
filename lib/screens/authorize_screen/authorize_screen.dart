import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/screens/groups_screen/groups_screen_provider.dart';
import 'package:tp/screens/sing_up_screen/sing_up_screen_provider.dart';
import 'package:tp/widgets/Creating_group/AppBar_V2_widget.dart';
import 'package:tp/widgets/Creating_group/form.dart';
import 'package:tp/widgets/button.dart';
import 'package:tp/widgets/pj_colors.dart';
import 'package:tp/widgets/text_styles.dart';
import 'cubit/cb_authorize_screen.dart';
import 'cubit/st_authorize_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthorizeScreen extends StatefulWidget {
  const AuthorizeScreen({Key? key}) : super(key: key);

  @override
  _AuthorizeScreenState createState() => _AuthorizeScreenState();
}

class _AuthorizeScreenState extends State<AuthorizeScreen> {
  TextEditingController _loginController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: NetworkImage(
              "https://yandex.ru/images/search?cbir_id=213996%2FWAh62hcelKSvvNmRTuWH6g8410&pos=0&rpt=imageview&img_url=https%3A%2F%2Fkrot.info%2Fuploads%2Fposts%2F2021-12%2F1638491004_59-krot-info-p-plyazh-pesok-krasivie-foto-66.jpg&from=tabbar&cbir_page=similar&url=https%3A%2F%2Favatars.mds.yandex.net%2Fget-images-cbir%2F213996%2FWAh62hcelKSvvNmRTuWH6g8410%2Forig"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: BlocConsumer<CbAuthorizeScreen, StAuthorizeScreen>(
            listener: (context, state) {
              if(state is StAuthorizeScreenPushMain){
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (BuildContext context) {
                      return const GroupsScreenProvider();
                    },
                  ),
                );
              }
            },
            builder: (context, state) {
              if (state is StAuthorizeScreenLoading) {
                return Column(
                  children: const [
                    Center(
                      child: CupertinoActivityIndicator(),
                    ),
                  ],
                );
              }
              if (state is StAuthorizeScreenLoaded) {
                return Column(
                  children: [
                    SizedBox(height: 330.w),
                    Center(
                      child: Text(
                        'AUTHORIZATION',
                        style: PjTextStyles.auth,
                      ),
                    ),
                    SizedBox(height: 30.w),
                    FormWidget(
                      controller: _loginController,
                      hintText: 'login',
                    ),
                    SizedBox(height: 30.w),
                    FormWidget(
                      controller: _passwordController,
                      hintText: 'password',
                    ),
                    SizedBox(height: 30.w),
                    PjButton(
                      text: 'LOG IN',
                      onTap: () {
                        BlocProvider.of<CbAuthorizeScreen>(context).auth(
                            _loginController.text, _passwordController.text);
                      },
                    ),
                    SizedBox(height: 20.w),
                    PjButton(
                      text: 'SING UP',
                      onTap: () {
                        Navigator.of(context).push(
                          CupertinoPageRoute(
                            builder: (BuildContext context) {
                              return const SingUpScreenProvider();
                            },
                          ),
                        );
                      },
                      isWhiteButton: true,
                    ),
                  ],
                );
              }
              if (state is StAuthorizeScreenError) {
                return Container(color: Colors.red);
              }
              return Container(color: Colors.grey);
            },
          ),
        ),
      ),
    );
  }
}
