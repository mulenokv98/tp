import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'authorize_screen.dart';
import 'cubit/cb_authorize_screen.dart';
import 'cubit/st_authorize_screen.dart';

class AuthorizeScreenProvider extends StatelessWidget {
  const AuthorizeScreenProvider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CbAuthorizeScreen>(
      create: (context) => CbAuthorizeScreen(),
      child: const AuthorizeScreen(),
    );
  }
}    
    