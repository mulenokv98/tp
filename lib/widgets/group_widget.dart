import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:tp/models/groups_model.dart';
import 'package:tp/screens/creating_group_screen/creating_group_screen_provider.dart';
import 'package:tp/screens/groups_screen/cubit/cb_groups_screen.dart';
import 'package:tp/widgets/pj_colors.dart';
import 'package:tp/widgets/text_styles.dart';

class GroupWidget extends StatelessWidget {
  GroupsModel group;

  GroupWidget({Key? key, required this.group}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Container(
        height: 140.w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.w),
          border: Border.all(
            width: 3.w,
            color: PjColors.buttonBlue,
          ),
          color: Colors.grey.shade200,
        ),
        child: Row(
          children: [
            SizedBox(width: 20.w),
            CircleAvatar(
              radius: 50.w,
              backgroundColor: PjColors.buttonBlue,
            ),
            SizedBox(width: 30.w),
            Text(
              group.name!,
              style: PjTextStyles.buttonWhite,
            ),
            const Spacer(),
            GestureDetector(
              child: Icon(
                Icons.edit,
                color: Colors.grey,
                size: 60.w,
              ),
              onTap: () {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (BuildContext context) {
                      return CreatingGroupScreenProvider(groupId: group.id!,);
                    },
                  ),
                );
              },
            ),
            SizedBox(width: 20.w),
            GestureDetector(
              child: Icon(
                Icons.delete,
                color: Colors.red,
                size: 60.w,
              ),
              onTap: () {
                Get.dialog(
                  Container(
                    width: 300.w,
                    height: 200.w,
                    color: PjColors.getGrey600,
                    child: Text(
                      'Точно удалить?',
                      style: PjTextStyles.button,
                    ),
                  ),
                );
                //BlocProvider.of<CbGroupsScreen>(context).deleteGroups(group.id!);
              },
            ),
            SizedBox(width: 30.w),
          ],
        ),
      ),
    );
  }
}
