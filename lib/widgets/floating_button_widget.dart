import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/widgets/pj_colors.dart';

class FloatingButtonWidget extends StatelessWidget {
  Function()? onPressed;

  FloatingButtonWidget({Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130.w,
      width: 130.w,
      child: FloatingActionButton(
        onPressed: () {
          if (onPressed != null){
            onPressed!();
          }
        },
        backgroundColor: PjColors.buttonBlue,
        child: Icon(
          Icons.add,
          size: 70.w,
        ),
      ),
    );
  }
}
