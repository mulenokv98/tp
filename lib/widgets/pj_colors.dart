import 'package:flutter/material.dart';

class PjColors{
  static Color getDarkBlue = Color.fromRGBO(67,78,181, 1);
  static Color buttonBorder = Color.fromRGBO(112,167,255, 1);
  static Color buttonBlue = Color.fromRGBO(96,112,255, 1);
  static Color getBrightBlue = Colors.blue;
  static Color getWhite = Colors.white;
  static Color getBGPColor = Color.fromRGBO(243,243,243, 1);
  static Color getGrey600 = Color.fromRGBO(103,103,103, 1);
}