import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/widgets/text_styles.dart';

import '../pj_colors.dart';

class PjAppBar extends StatelessWidget with PreferredSizeWidget {
  String title;
  bool isBackIcon;

  PjAppBar({Key? key, this.title = '', this.isBackIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: PjColors.buttonBlue,
      leading: isBackIcon ? IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          Icons.arrow_back,
          size: 50.w,
          color: Colors.white,
        ),
      ) : null,
      title: Text(
        title,
        style: PjTextStyles.button,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
