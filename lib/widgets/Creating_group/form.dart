import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/widgets/text_styles.dart';

import '../pj_colors.dart';

class FormWidget extends StatefulWidget {
  final FocusNode? focusNode;
  final String hintText;
  final TextEditingController controller;
  final String? Function(String)? wrongPass;
  final bool isConfPass;
  final int size;

  FormWidget({
    Key? key,
    this.focusNode,
    required this.hintText,
    required this.controller,
    this.wrongPass,
    this.isConfPass = false,
    this.size = 0,
  }) : super(key: key);

  @override
  _FormWidgetState createState() => _FormWidgetState();
}

class _FormWidgetState extends State<FormWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: SizedBox(
        width: widget.size == 0 ? null : widget.size.w,
        child: TextFormField(
          controller: widget.controller,
          focusNode: widget.focusNode,
          style: TextStyle(
            color: Colors.black,
            fontSize: 25.w,
            fontWeight: FontWeight.w400,
          ),
          decoration: InputDecoration(
            label: Text(
              widget.hintText,
              style: PjTextStyles.form,
            ),
            floatingLabelBehavior: FloatingLabelBehavior.never,
            contentPadding: EdgeInsets.symmetric(
              vertical: 12.w,
              horizontal: 16.w,
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.w),
              borderSide: BorderSide(
                  color: widget.size == 0 ? PjColors.buttonBorder : PjColors.getGrey600),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.w),
              borderSide: BorderSide(
                  color: widget.size == 0 ? PjColors.buttonBorder : PjColors.getGrey600),
            ),
            hintStyle: TextStyle(
              color: PjColors.getWhite,
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
