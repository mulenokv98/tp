import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/widgets/button.dart';
import 'package:tp/widgets/floating_button_widget.dart';

import '../pj_colors.dart';
import 'AppBar_V2_widget.dart';
import 'form.dart';

class CreatingGroupPage extends StatefulWidget {
  @override
  _CreatingFormPageState createState() => _CreatingFormPageState();
}

class _CreatingFormPageState extends State<CreatingGroupPage> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PjAppBar(
        title: 'Добавление группы',
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 20.w,
              top: 35.w,
              bottom: 15.w,
            ),
            child: Row(
              children: [
                Container(
                  constraints: BoxConstraints(
                      minHeight: 80, minWidth: 80, maxHeight: 80, maxWidth: 80),
                  decoration: BoxDecoration(
                    border: Border.all(color: PjColors.getDarkBlue),
                    borderRadius: BorderRadius.all(Radius.circular(40)),
                    color: PjColors.getBGPColor,
                  ),
                  //child: Image(),
                ),
                SizedBox(width: 20.w),
                //FormWidget(hintText: 'Название', controller: _controller)
              ],
            ),
          ),
          Divider(color: PjColors.getGrey600),
          Container(
            padding: EdgeInsets.all(10.w),
            margin: EdgeInsets.all(20.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: PjColors.getGrey600),
            ),
            child: Column(
              children: [
                Text(
                  "Настройки приватности",
                  style: TextStyle(fontSize: 25),
                ),
                SizedBox(height: 30.w),
                Container(
                  padding: EdgeInsets.only(
                      top: 5, left: 20.w, right: 20.w, bottom: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: PjColors.getGrey600),
                  ),
                  child: Column(
                    children: [
                      Text("Публичная доска",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.w500)),
                      SizedBox(height: 5),
                      Text(
                        "Если вы выберите публичный тип, то другие участники смогут свободно просматривать эту доску",
                        style: TextStyle(
                          fontSize: 17,
                          color: PjColors.getGrey600,
                        ),
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.only(
                      top: 5, left: 20.w, right: 20.w, bottom: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: PjColors.getGrey600),
                  ),
                  child: Column(
                    children: [
                      Text("Приватная доска",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.w500)),
                      SizedBox(height: 5),
                      Text(
                        "Если вы выберите приватный тип, то другие участники не смогут свободно просматривать эту доску",
                        style: TextStyle(
                          fontSize: 17,
                          color: PjColors.getGrey600,
                        ),
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          PjButton(text: 'Создать', onTap: () {}),
          SizedBox(
            height: 60.w,
          )
        ],
      ),
    );
  }
}
