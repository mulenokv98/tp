import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/widgets/text_styles.dart';
import 'pj_colors.dart';

class DrawerWidget extends StatelessWidget {
  String user;
  String userEmail;

  DrawerWidget(
      {Key? key,
      this.user = 'User User Userovich',
      this.userEmail = 'test@mail.com'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: PjColors.buttonBlue,
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: 400.w,
            child: DrawerHeader(
                decoration: BoxDecoration(color: PjColors.getDarkBlue),
                child: Padding(
                  padding: EdgeInsets.only(left: 10.w),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              backgroundColor: PjColors.buttonBlue,
                              radius: 50.w,
                            ),
                            SizedBox(width: 20),
                            Flexible(
                              child: Text(
                                user,
                                style: TextStyle(
                                    fontSize: 20, color: PjColors.getWhite),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20),
                        Text(
                          userEmail,
                          style:
                              TextStyle(color: PjColors.getWhite, fontSize: 18),
                        ),
                        SizedBox(height: 20),
                        Text(
                          "info:  blah blah blah blah blah blah blah",
                          style:
                              TextStyle(color: PjColors.getWhite, fontSize: 18),
                        )
                      ]),
                )),
          ),
          Column(
            children: List.generate(
              3,
              (index) => Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 15.w),
                child: Container(
                  height: 100.w,
                  decoration: BoxDecoration(
                    color:
                        index == 0 ? PjColors.getDarkBlue : PjColors.buttonBlue,
                    borderRadius: BorderRadius.circular(18.w),
                    border: Border.all(
                      width: 3.w,
                      color: PjColors.getDarkBlue,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: 20.w),
                      Icon(
                        index == 0
                            ? Icons.group
                            : index == 1
                                ? Icons.settings
                                : Icons.info,
                        color: PjColors.getWhite,
                        size: 60.w,
                      ),
                      SizedBox(width: 40.w),
                      Text(
                          index == 0
                              ? "Группы"
                              : index == 1
                                  ? "Настройки"
                                  : "О нас",
                          style: PjTextStyles.button),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 20.w),
        ],
      ),
    );
  }
}
