import 'package:flutter/material.dart';

class PjTextStyles{
  static TextStyle auth = TextStyle(fontSize: 30);
  static TextStyle button = TextStyle(fontSize: 20, color: Colors.white);
  static TextStyle buttonWhite = TextStyle(fontSize: 20, color: Colors.black);
  static TextStyle form = TextStyle(fontSize: 20, color: Colors.grey);
}