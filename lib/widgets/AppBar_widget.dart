import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReusableAppBar {
  static getAppBar(String title) {
    return AppBar(
      centerTitle: true,
      title:Text(title),
      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.search,
            //size: 35,
          ),
        ),
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.notifications,
              //size: 35,
            )),
      ],
    );
  }
}
