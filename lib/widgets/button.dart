import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tp/widgets/pj_colors.dart';
import 'package:tp/widgets/text_styles.dart';

class PjButton extends StatelessWidget {
  String text;
  bool isWhiteButton;
  Function() onTap;

  PjButton(
      {Key? key,
      this.isWhiteButton = false,
      required this.text,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 72.w,
        width: 680.w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.w),
          color: isWhiteButton ? PjColors.getWhite : PjColors.buttonBlue,
          border: Border.all(
            width: 3.w,
            color: PjColors.buttonBorder,
          ),
        ),
        child: Center(
          child: Text(
            text,
            style: isWhiteButton ? PjTextStyles.buttonWhite : PjTextStyles.button,
          ),
        ),
      ),
    );
  }
}
