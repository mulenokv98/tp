Rails.application.routes.draw do

  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  get "/logout", to: "sessions#destroy"
  
  namespace :api do
    namespace :v1 do
      resources :users do
        resources :groups do
          post :add_user_to_group
          resources :cards
        end
      end
    end
  end
end
