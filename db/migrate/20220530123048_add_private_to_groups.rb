class AddPrivateToGroups < ActiveRecord::Migration[7.0]
  def change
    add_column :groups, :private, :boolean, default: true
  end
end
