class AddNameToCards < ActiveRecord::Migration[7.0]
  def change
    add_column :cards, :name, :string
  end
end
