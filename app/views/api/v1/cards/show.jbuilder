json.id @card.id
json.group_id @card.group_id
json.name @card.name
json.undercards @undercards do |undercard|
  json.id undercard.id
  json.title undercard.title
  json.description undercard.description
  json.card_id undercard.card_id
end