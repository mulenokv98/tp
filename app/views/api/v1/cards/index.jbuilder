json.cards @cards do |card|
  json.id card.id
  json.name card.name
  json.group_id card.group_id
end