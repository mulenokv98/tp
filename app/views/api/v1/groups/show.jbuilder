json.id @group.id
json.name @group.name
json.users @users do |user|
  json.id user.id
  json.fullname user.fullname
  json.email user.email
  json.info user.info
end
json.private @group.private