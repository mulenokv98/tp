module Api
  module V1
    class CardsController < ApiController
      def index
        @cards = Card.where(group_id: params[:group_id])
      end

      def show
        @card = Card.find(params[:id])
        @undercards = Undercard.where(card_id: params[:id])
      end
    end
  end
end