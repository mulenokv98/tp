module Api
  module V1
    class GroupsController < ApiController

      def index
        if @groups = User.find(params[:user_id]).groups
          render :index
        else
          render json: 404
        end
      end

      def create
        @group = Group.new(group_params)
        if @group.save && UserGroup.create(group_id: @group.id, user_id: params[:user_id])
          render :create
        else
          render :error
        end
      end

      def show
        @group = Group.find(params[:id])
        @users = @group.users
      end

      def update
        Group.find(params[:id]).update(group_params)
      end

      def destroy
        if Group.find(params[:id]).destroy
          render :destroy
        else
          render :error
        end
      end

      def add_user_to_group
        if !UserGroup.find_by(group_id: params[:id], user_id: params[:user_id]).present?
          if UserGroup.create(group_id: params[:id], user_id: params[:user_id])
            render :create
          else
            render :error
          end
        else
          render json: 'Этот пользователь уже привзяан к группе'
        end
      end

      private

      def group_params 
        params.require(:group).permit(:name, :private, :user_id)
      end
    end
  end
end