module Api
  module V1
    class UsersController < ApiController
      def index
        @users = User.all
      end

      def show
        @user = User.find(params[:id])
      end

      def create
        @user = User.new(user_params)
        if @user.save 
          session[:user_id] = @user.id
        else
          render :error
        end
      end

      def update
        @user.update(user_params)
      end

      def destroy
        @user.destroy
      end

      def user_params
        params.require(:user).permit(:email, :login, :password, :info, :fullname)
      end
    end
  end
end