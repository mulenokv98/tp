class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    @user = User.find_by(login: params[:login])
    if @user
      if @user.authenticate(params[:password])
        session[:user_id] = @user.id
        render json: @user.id
      else
        render json: 'Неверный пароль'
      end
    else
      render json: 'Неверный логин'
    end
  end
end